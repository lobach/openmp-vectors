#include <iostream>
#include <omp.h>

#define VECTOR_SIZE 1000000000
#define OUTPUT_ENABLED_SYNC false
#define OUTPUT_ENABLED_PARALLEL false

int resultParallel;
int resultSync;

double execute(const char *vec1, const char *vec2);

void execute(const char *vec1, const char *vec2, int runCount);

void executeParallel(const char *vec1, const char *vec2, int runCount);

double executeParallel(const char *vec1, const char *vec2);

int main()
{
    char *vec1 = new char[VECTOR_SIZE];
    char *vec2 = new char[VECTOR_SIZE];

    for (int i = 0; i < VECTOR_SIZE; i++)
    {
        vec1[i] = 1;
        vec2[i] = 1;
    }

    execute(vec1, vec2, 100);
    executeParallel(vec1, vec2, 100);

    delete[] vec1;
    delete[] vec2;

    return 0;
}

void executeParallel(const char *vec1, const char *vec2, int runCount)
{
    double totalTime = 0;
    for (int i = 0; i < runCount; ++i)
        totalTime += executeParallel(vec1, vec2);

    //totalTime /= runCount;
    std::cout << "Parallel: " << totalTime << " s" << " Result: " << resultParallel << std::endl;
}

double executeParallel(const char *vec1, const char *vec2)
{
    int threadCount = omp_get_max_threads();
    int *result = new int[threadCount];
    for (int i = 0; i < threadCount; i++)
        result[i] = 0;

    int chunkSize = (VECTOR_SIZE / threadCount) + 1;
    double startTime = omp_get_wtime();
#pragma omp parallel
    {
        int localResult = 0;
        int threadId = omp_get_thread_num();
        int chunkStart = threadId * chunkSize;
        int chunkEnd = chunkStart + chunkSize;
        if (chunkEnd >= VECTOR_SIZE)
            chunkEnd = VECTOR_SIZE;

        for (int i = chunkStart; i < chunkEnd; i++)
            localResult += vec1[i] * vec2[i];

        result[threadId] = localResult;
    }

    int totalResult = 0;
    for (int i = 0; i < threadCount; i++)
        totalResult += result[i];

    double endTime = omp_get_wtime();
    double totalTime = endTime - startTime;

    if (OUTPUT_ENABLED_PARALLEL)
        std::cout << totalResult << std::endl;

    resultParallel = totalResult;

    delete[] result;

    return totalTime;
}

void execute(const char *vec1, const char *vec2, int runCount)
{
    double totalTime = 0;
    for (int i = 0; i < runCount; ++i)
        totalTime += execute(vec1, vec2);

    //totalTime /= runCount;
    std::cout << "Sync: " << totalTime << " s" << " Result: " << resultSync << std::endl;
}

double execute(const char *vec1, const char *vec2)
{
    int result = 0;
    double startTime = omp_get_wtime();
    for (int i = 0; i < VECTOR_SIZE; i++)
        result += vec1[i] * vec2[i];

    double endTime = omp_get_wtime();
    double totalTime = endTime - startTime;

    if (OUTPUT_ENABLED_SYNC)
        std::cout << result << std::endl;

    resultSync = result;

    return totalTime;
}